extends Object


#projects (value) onto (normal)'s plane, making a Vector2
#the Vector2's positive x axis is in the direction of (forward)
static func flatten(value:Vector3, normal:Vector3, forward:Vector3) -> Vector2:
	var p := Plane(normal, 0.0)
	value = p.project(value)
	forward = p.project(forward)
	var out := Vector2(value.length(), 0)
	return out.rotated( forward.signed_angle_to(value, normal) )


#Similar to Vector3.signed_angle_to except it returns the angle around (axis)
#rather than a signed Vector3.angle_to
static func signed_angle_to(from:Vector3, to:Vector3, axis:Vector3) -> float:
	var forward := Vector3.FORWARD
	if axis.normalized() == Vector3.FORWARD or axis.normalized() == Vector3.BACK:
		forward = Vector3.UP
	var from2 := flatten(from, axis, forward)
	var to2 := flatten(to, axis, forward)
	return from2.angle_to(to2)


#Plane.project except with a plane defined by (normal)
static func project_on_plane(value:Vector3, normal:Vector3) -> Vector3:
	return Plane(normal, 0.0).project(value)


#Returns the orthogonal projection of (value) into positive space of (normal)'s plane.
static func clip(value:Vector3, normal:Vector3) -> Vector3:
	return value if normal.dot(value) >= 0.0 else project_on_plane(value, normal)

