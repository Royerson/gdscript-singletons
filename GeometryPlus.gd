extends Node


#takes CollisionPolygon2D or CollisionShape2D
#returns true if either shape is fully enclosed in the other
static func owners_enclosed(owner_a, owner_b) -> bool:
	if _does_owner_own_line(owner_a) or _does_owner_own_line(owner_b):
		return false
	
	assert(owner_a is CollisionPolygon2D or owner_a is CollisionShape2D)
	assert(owner_b is CollisionPolygon2D or owner_b is CollisionShape2D)
	owner_a = owner_to_PoolVector2Array_transformed(owner_a)
	owner_b = owner_to_PoolVector2Array_transformed(owner_b)
	return polygons_2d_enclosed(owner_a, owner_b)


static func _does_owner_own_line(owner) -> bool:
	var shape = owner.get("shape")
	return shape is LineShape2D or shape is RayShape2D or shape is SegmentShape2D


#returns true if either polygon is fully enclosed in the other
static func polygons_2d_enclosed(polygon_a:PoolVector2Array, polygon_b:PoolVector2Array) -> bool:
	if !Geometry.clip_polygons_2d(polygon_a, polygon_b):
		return true
	if !Geometry.clip_polygons_2d(polygon_b, polygon_a):
		return true
	return false
##faster but doesn't work with rotations due to floating point bs (I think)
#static func polygons_2d_enclosed(polygon_a:PoolVector2Array, polygon_b:PoolVector2Array) -> bool:
#	var intersect = Geometry.intersect_polygons_2d(polygon_a, polygon_b)
#	if Input.is_action_just_pressed("ui_down"):
#		breakpoint
#	if !intersect:
#		return false
#	var intersect_array = Array(intersect[0])
#	intersect_array.sort()
#	var polygon_a_array = Array(polygon_a)
#	polygon_a_array.sort()
#	var polygon_b_array = Array(polygon_b)
#	polygon_b_array.sort()
#	return intersect_array == polygon_a_array or intersect_array == polygon_b_array


static func owner_to_PoolVector2Array_transformed(owner) -> PoolVector2Array:
	var poly = owner_to_PoolVector2Array(owner)
	var trans = owner.global_transform
	for i in poly.size():
		poly[i] = trans.xform(poly[i])
	return poly


static func owner_to_PoolVector2Array(owner) -> PoolVector2Array:
	assert(owner is CollisionPolygon2D or owner is CollisionShape2D)
	if owner is CollisionPolygon2D:
		return owner.polygon
	assert("shape" in owner)
	return Shape2D_to_PoolVector2Array(owner.shape)


static func Shape2D_to_PoolVector2Array(shape:Shape2D, segments:=24) -> PoolVector2Array:
	var out : PoolVector2Array = []
	match shape.get_class():
#		#this doesn't work for collision
#		"SegmentShape2D":
#			out = [shape.a, shape.b, shape.a]
		"RectangleShape2D":
			out.append( -shape.extents )
			out.append( shape.extents * Vector2(1,-1) )
			out.append( shape.extents )
			out.append( shape.extents * Vector2(-1,1) )
		"CircleShape2D":
			var step = PI * 2 / segments
			var current = Vector2(shape.radius, 0)
			for _i in segments:
				out.append(current)
				current = current.rotated(step)
		"CapsuleShape2D":
			if segments % 2 != 0:
				segments += 1
			var step = PI * 2 / segments
			for start in [1, -1]:
				var current = start * Vector2(shape.radius, 0)
				var offset = start * Vector2(0, shape.height/2)
				# warning-ignore:integer_division
				for _i in segments/2 + 1:
					out.append(current + offset)
					current = current.rotated(step)
		_:
			push_error("invalid shape: " + shape.get_class())
	return out
